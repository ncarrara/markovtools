/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mc;

import Jama.Matrix;
import tool.Alea;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 * @param <E>
 */
public class MarkovChain<E> {

    protected final E[] etats;
    
    protected Matrix  mdistribution;
    
    protected final double[][] transitions;
    
    protected final Matrix mtransitions;
    
    protected final double[][] transposeTransitions;
    
    protected int iterations;
    
    protected int indiceCourant;
    
    protected final double[] distributionDepart;
    
    public MarkovChain(E[] etats, double[][] transitions, double[] distribution_initiale){
        distributionDepart = distribution_initiale;
        this.transitions = transitions;
        this.etats = etats;
        iterations = 0;
//        distribution = new double[etats.length];
        transposeTransitions = new double[etats.length][etats.length];
        for (int i = 0; i < transitions.length; i++) {
            for (int j = 0; j < transitions.length; j++) {
                transposeTransitions[i][j] = transitions[j][i];
            }
        }
        this.mdistribution = new Matrix(distribution_initiale, distribution_initiale.length);
        this.mtransitions = new Matrix(transitions);
    }
    
    public E getEtatCourant(){
        return etats[indiceCourant];
    }
    
    public void reset(){
        indiceCourant = 0;
        iterations = 0;
        this.mdistribution = new Matrix(distributionDepart, distributionDepart.length);
    }
    
    /**
     * marche sur la chaine
     * @return l'indice de l'état suivant
     */
    public int walk(){
        mdistribution = mtransitions.times(mdistribution);
        iterations++;
//        System.out.print(iterations+" : "+etats[indiceCourant]+" -> ");
        indiceCourant = Alea.uniforme(transposeTransitions[indiceCourant]);
//        System.out.println(""+etats[indiceCourant]);
        return indiceCourant;
            
    }
    
    public double[] getDistribution(){
//        return mdistribution.getArray();
        int l = mdistribution.getArray().length;
        double[] distrib = new double[l];
//        System.out.println("mdistribution : "+Arrays.deepToString(mdistribution.getArray()));
        for (int i = 0; i < l; i++) {
            distrib[i] = (mdistribution.getArray()[i])[0];
        }
//        System.out.println(""+Arrays.deepToString(mdistribution.getArray()));
        return distrib;
    }
    
    public String toString(){
        String s = "\n\t";
        for (int i = 0; i < etats.length; i++) {
            s+= etats[i]+"\t";
        }
        s+= "\n";
        for (int i = 0; i < etats.length; i++) {
            s+= etats[i]+ "\t";
            for (int j = 0; j < etats.length; j++) {
                s += String.format( "%.2f",transitions[i][j]) +"\t";
            }
            s+="\n";
        }
        
        return s;
        
    }
    
}
