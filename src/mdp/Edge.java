/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdp;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr & VOGEL Thomas <thomas-vogel@wanadoo.fr>
 */
public final class Edge {

    int nextState;
    double probaTransition;
    double reward;

    public Edge(int nextState, double probaTransition, double reward) {
        this.nextState = nextState;
        this.probaTransition = probaTransition;
        this.reward = reward;
    }
    
    public String toString(){
        return nextState+" with "+probaTransition+" "+" -> "+reward;
    }
}
