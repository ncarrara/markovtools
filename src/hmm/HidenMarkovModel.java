/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hmm;

import java.util.Arrays;
import mc.MarkovChain;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class HidenMarkovModel<E> extends MarkovChain<E> {

    /**
     *
     * 4 etats et 2 observations possible
     *
     * Etats 0.3 0.2 0.4 0.1 Observation 0.1 0.3 0.6 0.0
     *
     * exemple : la probabilité qu'on observe 1 si l'état est 2 est de 60%
     */
    private final double[][] probasObservationEtat;

    /**
     * etat de croyance pr l'état 0 1 2 ...
     */
    private double[] currentBeliefs;

    
    
    public HidenMarkovModel(E[] etats, double[][] transitions, double[][] probasObversationEtat, double[] distribution_initiale) {
        super(etats, transitions, distribution_initiale);
        this.probasObservationEtat = probasObversationEtat;
        currentBeliefs = Arrays.copyOf(distribution_initiale, distribution_initiale.length);
    }

    @Override
    public void reset(){
        super.reset();
        currentBeliefs =Arrays.copyOf(distributionDepart, distributionDepart.length);
    }
    
    public double[] computeNextBelief(int indiceObservation) {
        // evolution état
        double[] priors = new double[etats.length];
        for (int e = 0; e < etats.length; e++) {
            for (int e_before = 0; e_before < etats.length; e_before++) {
                priors[e] += transitions[e][e_before] * currentBeliefs[e_before];
            }
        }
        // prise en compte des observations
        double facteur = 0.0;
        for (int e = 0; e < etats.length; e++) {
            currentBeliefs[e] = priors[e] * probasObservationEtat[indiceObservation][e];
            facteur += currentBeliefs[e];
        }
        // on normalise
//        System.out.println("facteur : "+facteur);
        facteur = 1./facteur;
        for (int e = 0; e < etats.length; e++) {
            currentBeliefs[e] = facteur * currentBeliefs[e];
        }
        return getCurrentBeliefs();
    }

    /**
     * @return the currentBeliefs
     */
    public double[] getCurrentBeliefs() {
        return currentBeliefs;
    }

    /**
     * @return the probasObservationEtat
     */
    public double[][] getProbasObservationEtat() {
        return probasObservationEtat;
    }
}
