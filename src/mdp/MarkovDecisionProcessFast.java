/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdp;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * La question est : je le représente comme une couche de chaine de markov, ou
 * je refais complétement la structure ? Imo c'est plus simple de tout refaire.
 * Et surement plus efficace.
 *
 * PS : il suffit de laiser les valeurs de transition à zéro si des actions pour
 * certains états n'existent pas.
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr> & VOGEL Thomas <thomas-vogel@wanadoo.fr>
 */
public final class MarkovDecisionProcessFast {

    private List<Action>[] states;

    /**
     *
     * @param states
     */
    public MarkovDecisionProcessFast(List<Action>[] states) {
        this.states = states;
    }

    public void run(){
        // on s'en moque on ne simule pas ...
    }
    
    /**
     *
     * @param pi i : state j : action
     * @param theta stop criterion
     * @param gamma discount factor
     * @return
     */
    public double[] policyEvalutation(Policy pi, double theta, double gamma) {
        double[] V = new double[states.length];
        double delta = Double.MAX_VALUE;
        while (!(delta < theta)) {
            delta = 0.0;
            for (int s = 0; s < states.length; s++) {
                double v = V[s];
                double sumVs = 0.0;
                for (Action a : states[s]) {
                    double p;
                    if ((p = pi.getProba(s, a)) > 0) {
                        double sum = 0.0;
                        for (Edge e : a.edges) {
                            sum += e.probaTransition * (e.reward + gamma * V[e.nextState]);
                        }
                        sumVs += p * sum;
                    }
                }
                V[s] = sumVs;
                delta = Math.max(delta, Math.abs(v - V[s]));
            }
        }
        return V;
    }

    private double[] V_from_value_iteration;

    /**
     *
     * @param theta stop criterion
     * @param gamma discount factor
     * @return pi
     */
    public Policy valueIterationV(double theta, double gamma) {
        Policy pi = new Policy(states.length);
        double[] V = new double[states.length];
        double delta = Double.MAX_VALUE;
        while (!(delta < theta)) {
            delta = 0.0;
            for (int s = 0; s < states.length; s++) {
                double v = V[s];
                double maxVs = Double.MIN_VALUE;
                for (Action a : states[s]) {
                    double sum = 0.0;
                    for (Edge e : a.edges) {
                        sum += e.probaTransition * (e.reward + gamma * V[e.nextState]);
                    }
                    maxVs = Math.max(maxVs, sum);
//                    if (maxVs < sum) {
//                        maxVs = sum;
//                        pi.clearFromState(s);
//                        pi.add(s, a, 1.0);
//                    }
                }
                V[s] = maxVs;
                delta = Math.max(delta, Math.abs(v - V[s]));
            }
        }

        /**
         * output a deterministic policy TT such that ... On fait une autre
         * boucle sans mise à jour des Vs mais j'ai pas l'impression que ça
         * change grand chose
         */
        for (int s = 0; s < states.length; s++) {
//            System.out.println("S : "+s);
            double maxVs = Double.MIN_VALUE;
            Action actionToDo = null;
            // on cherche la meilleur action pr cet état
            for (Action a : states[s]) {
                double sum = 0.0;
                // on cacul l'esperance du choix de cette action
//                System.out.println("---------------------");
                for (Edge e : a.edges) {
//                    System.out.println(""+e.probaTransition+" -> "+e.nextState+" +++"+e.reward);
                    sum += e.probaTransition * (e.reward + gamma * V[e.nextState]);
                }
//                System.out.println("---------------------");
                // si l'expérence de gain st meilleur on la garde
                if (maxVs < sum) {
                    maxVs = sum;
                    actionToDo = a;
                }
            }
//            System.out.println("actionToDo : " + actionToDo);
            if (actionToDo != null) {
                pi.add(s, actionToDo, 1.0);
            }
        }
//        System.out.println("" + pi);
        V_from_value_iteration = V;
        return pi;
    }

//    /**
//     *
//     * @param theta stop criterion
//     * @param gamma discount factor
//     * @return pi
//     */
//    public Policy valueIterationQ(double theta, double gamma) {
//        Policy pi = new Policy(states.length);
//        double[] V = new double[states.length];
//        double delta = Double.MAX_VALUE;
//        while (!(delta < theta)) {
//            delta = 0.0;
//            for (int s = 0; s < states.length; s++) {
//                double v = V[s];
//                double maxVs = Double.MIN_VALUE;
//                for (Action a : states[s]) {
//                    double sum = 0.0;
//                    for (Edge e : a.edges) {
//                        sum += e.probaTransition * (e.reward + gamma * V[e.nextState]);
//                    }
//                    if (maxVs < sum) {
//                        maxVs = sum;
//                        pi.clearFromState(s);
//                        pi.add(s, a, 1.0);
//                    }
//                }
//                V[s] = maxVs;
//                delta = Math.max(delta, Math.abs(v - V[s]));
//            }
//        }
//        V_from_value_iteration = V;
//        return pi;
//    }
    /**
     * @return the V_from_value_iteration
     */
    public double[] getV_from_value_iteration() {
        return V_from_value_iteration;
    }

    /**
     * @param V_from_value_iteration the V_from_value_iteration to set
     */
    public void setV_from_value_iteration(double[] V_from_value_iteration) {
        this.V_from_value_iteration = V_from_value_iteration;
    }

}
