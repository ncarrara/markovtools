/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr & VOGEL Thomas <thomas-vogel@wanadoo.fr>
 */
public class Policy {

    

    List<ActionProba>[] distributions;

    public Policy(List<ActionProba>[] distributions) {
        this.distributions = distributions;
    }

    public Policy(int nbEtat) {
        distributions = new List[nbEtat];
        for (int i = 0; i < nbEtat; i++) {
            distributions[i] = new ArrayList<>();
            
        }
    }
    
    public int size(){
        return distributions.length;
    }

    public void add(int s,Action a,double proba){
        List<ActionProba> distribution = distributions[s];
        ActionProba ap = new ActionProba(a, proba);
        int index;
        if((index = distribution.indexOf(ap))==-1){
            distribution.add(new ActionProba(a, proba));
        }else{
            distribution.set(index, ap);
        }
    }
    
    public List<ActionProba> getActionProba(int etat){
        return distributions[etat];
    }
    
    public void clearFromState(int state){
        distributions[state].clear();
    }
    
    // c'est moche
    public double getProba(int s, Action a)  {
        List<ActionProba> distribution = distributions[s];
        ActionProba ap = new ActionProba(a, Double.NaN);
        int index;
        if((index = distribution.indexOf(ap))==-1){
           return 0;
        }else{
            return distribution.get(index).getProba();
        }
    }

    
    public String toString(){
        String s = "";
        for (int i = 0; i < distributions.length; i++) {
            s+= "etat "+i+" -> action"+distributions[i]+"\n";
            
        }
        return s;
    }
}
