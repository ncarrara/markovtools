/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdp;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr & VOGEL Thomas <thomas-vogel@wanadoo.fr>
 */
public class ActionProba {

    private Action action;
    private double proba;

    public ActionProba(Action action, double proba) {
        this.action = action;
        this.proba = proba;
    }

    @Override
    public boolean equals(Object o) {
        return ((ActionProba) o).action.id == action.id;
    }

    @Override
    public String toString() {
        return action + " " + proba;
    }

    /**
     * @return the action
     */
    public Action getAction() {
        return action;
    }

    /**
     * @return the proba
     */
    public double getProba() {
        return proba;
    }
}
